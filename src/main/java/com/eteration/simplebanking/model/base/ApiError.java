package com.eteration.simplebanking.model.base;

import org.springframework.http.HttpStatus;

public class ApiError {
    private HttpStatus status;
    private String errorId;
    private String message;


    public HttpStatus getStatus() {
        return status;
    }

    public void setStatus(HttpStatus status) {
        this.status = status;
    }

    public String getErrorId() {
        return errorId;
    }

    public void setErrorId(String errorId) {
        this.errorId = errorId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    ApiError(ApiErrorBuilder apiErrorBuilder){
        this.status = apiErrorBuilder.status;
        this.errorId = apiErrorBuilder.errorId;
        this.message = apiErrorBuilder.message;
    }

    public static ApiErrorBuilder builder(){
        return new ApiErrorBuilder();
    }

    public static class ApiErrorBuilder{
        private HttpStatus status;
        private String errorId;
        private String message;

        public ApiErrorBuilder status(HttpStatus status) {
            this.status = status;
            return this;
        }

        public ApiErrorBuilder errorId(String errorId) {
            this.errorId = errorId;
            return this;
        }

        public ApiErrorBuilder message(String message) {
            this.message = message;
            return this;
        }

        public ApiError build(){
            return new ApiError(this);
        }


    }
}
