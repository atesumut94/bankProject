package com.eteration.simplebanking.model.base;

public final class GeneralEnum {


    public enum TransactionTypes {
        WITHDRAWAL_TRANSACTION("WithdrawalTransaction"),
        DEPOSITE_TRANSACTION("DepositTransaction"),
        BILLPAYMENT_TRANSACTION("PhoneBillPaymentTransaction");

        private String relation;

        TransactionTypes(String relation) {
            this.relation = relation;
        }

        public String getRelation() {
            return relation;
        }
    }

}
