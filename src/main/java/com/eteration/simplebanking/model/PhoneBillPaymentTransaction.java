package com.eteration.simplebanking.model;

import java.security.InvalidParameterException;

public class PhoneBillPaymentTransaction extends BillPaymentTransaction{

    public PhoneBillPaymentTransaction(double amount) {
        super(amount);
    }

    @Override
    public void processTransaction(Account account) {
        this.billTypePaymentParameterValidation();
        account.withdraw(this.getAmount());
    }

    private void billTypePaymentParameterValidation() {
        if (!this.getBillTypePaymentParameter().matches("^[0-9].*")) {
            throw new InvalidParameterException("Illegal BillTypePaymentParameter parameter should PHONE NUMBER");
        }
    }
}
