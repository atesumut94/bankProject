package com.eteration.simplebanking.model;


import com.eteration.simplebanking.exception.InsufficientBalanceException;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import java.util.Date;

// This class is a place holder you can change the complete implementation
public abstract class Transaction {

    private Date date;
    @DecimalMin("0.1")
    private double amount;
    private String type;
    private String approvalCode;

    public Transaction(double amount){
        this.amount = amount;
        this.date = new Date();
    }

    public Transaction() {

    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public abstract void processTransaction(Account account);

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }
}
