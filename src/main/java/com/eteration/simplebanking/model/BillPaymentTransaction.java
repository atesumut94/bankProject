package com.eteration.simplebanking.model;

import com.fasterxml.jackson.annotation.JsonIgnore;

public class BillPaymentTransaction extends Transaction{

    private String payee;

    private String billTypePaymentParameter;


    public BillPaymentTransaction(double amount) {
        super(amount);
    }

    public String getPayee() {
        return payee;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public String getBillTypePaymentParameter() {
        return billTypePaymentParameter;
    }

    public void setBillTypePaymentParameter(String billTypePaymentParameter) {
        this.billTypePaymentParameter = billTypePaymentParameter;
    }

    @Override
    public void processTransaction(Account account) {
        account.withdraw(this.getAmount());
    }


    @Override
    public String toString() {
        return "BillPaymentTransaction{" +
                "payee='" + payee + '\'' +
                '}';
    }
}
