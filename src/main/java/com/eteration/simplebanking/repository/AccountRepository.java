package com.eteration.simplebanking.repository;

import com.eteration.simplebanking.entity.AccountEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AccountRepository extends JpaRepository<AccountEntity, String> {
}
