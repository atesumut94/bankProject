package com.eteration.simplebanking.mapper;

import com.eteration.simplebanking.entity.TransactionEntity;
import com.eteration.simplebanking.model.BillPaymentTransaction;
import com.eteration.simplebanking.model.DepositTransaction;
import com.eteration.simplebanking.model.Transaction;
import com.eteration.simplebanking.model.WithdrawalTransaction;
import com.eteration.simplebanking.model.base.GeneralEnum;
import org.mapstruct.Mapper;
import org.mapstruct.Named;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring")
public interface TransactionMapper {

    TransactionEntity TransactionToTransactionEntity(Transaction transaction);

    DepositTransaction TransactionEntityToDepositTransaction(TransactionEntity transactionEntity);

    WithdrawalTransaction TransactionEntityToWithdrawlTransaction(TransactionEntity transactionEntity);

    BillPaymentTransaction TransactionEntityToBillPaymentTransaction(TransactionEntity transactionEntity);


    //TODO: need refactor, not bestpractice
    @Named("TransactionEntityListToTransactionList")
    default List<Transaction> TransactionEntityToTransaction(List<TransactionEntity> transactionEntityList) {
        List<Transaction> transactions = new ArrayList<>();
        for (TransactionEntity transactionEntity : transactionEntityList) {
            if (GeneralEnum.TransactionTypes.DEPOSITE_TRANSACTION.getRelation().equalsIgnoreCase(transactionEntity.getType())) {
                DepositTransaction depositTransaction = this.TransactionEntityToDepositTransaction(transactionEntity);
                transactions.add(depositTransaction);
            } else if (GeneralEnum.TransactionTypes.WITHDRAWAL_TRANSACTION.getRelation().equalsIgnoreCase(transactionEntity.getType())) {
                WithdrawalTransaction withdrawalTransaction = this.TransactionEntityToWithdrawlTransaction(transactionEntity);
                transactions.add(withdrawalTransaction);
            } else if (GeneralEnum.TransactionTypes.BILLPAYMENT_TRANSACTION.getRelation().equalsIgnoreCase(transactionEntity.getType())) {
                BillPaymentTransaction billPaymentTransaction = this.TransactionEntityToBillPaymentTransaction(transactionEntity);
                transactions.add(billPaymentTransaction);
            }
        }
        return transactions;
    }
}
