package com.eteration.simplebanking.mapper;

import com.eteration.simplebanking.dto.CreateAccountRequest;
import com.eteration.simplebanking.entity.AccountEntity;
import com.eteration.simplebanking.entity.TransactionEntity;
import com.eteration.simplebanking.model.Account;
import com.eteration.simplebanking.model.Transaction;
import com.eteration.simplebanking.model.WithdrawalTransaction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;
import org.mapstruct.control.DeepClone;

import java.util.ArrayList;
import java.util.List;

@Mapper(componentModel = "spring", mappingControl = DeepClone.class, uses = {TransactionMapper.class})
public interface AccountMapper {

    @Mapping(source = "accountEntity.transactions", target = "transactions", qualifiedByName = "TransactionEntityListToTransactionList")
    Account AccountEntityToAccount(AccountEntity accountEntity);

    AccountEntity AccountToAccountEntity(Account account);

    AccountEntity AccountRequestToAccountEntity(CreateAccountRequest createAccountRequest);


}
