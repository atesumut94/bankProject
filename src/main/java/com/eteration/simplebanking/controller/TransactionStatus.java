package com.eteration.simplebanking.controller;


// This class is a place holder you can change the complete implementation

import org.springframework.http.HttpStatus;

public class TransactionStatus {

    private String status;
    private String approvalCode;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getApprovalCode() {
        return approvalCode;
    }

    public void setApprovalCode(String approvalCode) {
        this.approvalCode = approvalCode;
    }

    public static TransactionStatus createTransactionStatus(HttpStatus httpStatus, String approvalCode){
        TransactionStatus response = new TransactionStatus();
        response.setStatus(httpStatus.name());
        response.setApprovalCode(approvalCode);
        return response;
    }
}
