package com.eteration.simplebanking.exception;

import javax.persistence.EntityNotFoundException;

public class AccountNotFoundException extends EntityNotFoundException {

    public AccountNotFoundException(String message){
        super(message);
    }
}
