package com.eteration.simplebanking.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.validation.constraints.NotBlank;

public class BillPaymentTransactionRequest extends TransactionRequest {

    @NotBlank
    private String payee;
    @NotBlank
    private String billType;
    @NotBlank
    private String billTypePaymentParameter;

    public String getPayee() {
        return payee;
    }

    public void setPayee(String payee) {
        this.payee = payee;
    }

    public String getBillType() {
        return billType;
    }

    public void setBillType(String billType) {
        this.billType = billType;
    }

    public String getBillTypePaymentParameter() {
        return billTypePaymentParameter;
    }

    public void setBillTypePaymentParameter(String billTypePaymentParameter) {
        this.billTypePaymentParameter = billTypePaymentParameter;
    }

}
