package com.eteration.simplebanking.services;


import com.eteration.simplebanking.controller.TransactionStatus;
import com.eteration.simplebanking.dto.BillPaymentTransactionRequest;
import com.eteration.simplebanking.dto.CreateAccountRequest;
import com.eteration.simplebanking.entity.AccountEntity;
import com.eteration.simplebanking.exception.AccountNotFoundException;
import com.eteration.simplebanking.mapper.AccountMapper;
import com.eteration.simplebanking.model.*;
import com.eteration.simplebanking.model.base.GeneralEnum.*;
import com.eteration.simplebanking.repository.AccountRepository;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;


// This class is a place holder you can change the complete implementation
@Service
public class AccountService {

    private final AccountRepository accountRepository;
    private final AccountMapper accountMapper;
    private final TransactionService transactionService;


    public AccountService(AccountRepository accountRepository, AccountMapper accountMapper, TransactionService transactionService) {
        this.accountRepository = accountRepository;
        this.accountMapper = accountMapper;
        this.transactionService = transactionService;
    }

    @Transactional
    public Account createAccount(CreateAccountRequest createAccountRequest) {
        this.validateCreateAccountRequest(createAccountRequest);
        AccountEntity accountEntity = accountMapper.AccountRequestToAccountEntity(createAccountRequest);
        return accountMapper.AccountEntityToAccount(accountRepository.save(accountEntity));
    }

    private void validateCreateAccountRequest(CreateAccountRequest createAccountRequest){
        if (createAccountRequest.getBalance() < 0.0) {
            throw new InvalidParameterException("Balance can not lower than 0");
        }
    }


    public Account findAccount(String accountNumber) {
        AccountEntity accountEntity = accountRepository.findById(accountNumber)
                .orElseThrow(() -> new AccountNotFoundException("Requested Account Not Found"));
        return accountMapper.AccountEntityToAccount(accountEntity);
    }

    @Transactional
    public TransactionStatus credit(String accountNumber, DepositTransaction depositTransaction) {
        this.validateTransactionRequest(depositTransaction);
        Account account = this.findAccount(accountNumber);
        this.fillTransaction(depositTransaction, TransactionTypes.DEPOSITE_TRANSACTION.getRelation());
        account.post(depositTransaction);
        accountRepository.save(accountMapper.AccountToAccountEntity(account));

        return TransactionStatus.createTransactionStatus(HttpStatus.OK, depositTransaction.getApprovalCode());
    }

    @Transactional
    public TransactionStatus debit(String accountNumber, WithdrawalTransaction withdrawalTransaction) {
        this.validateTransactionRequest(withdrawalTransaction);
        Account account = this.findAccount(accountNumber);
        this.fillTransaction(withdrawalTransaction, TransactionTypes.WITHDRAWAL_TRANSACTION.getRelation());
        account.post(withdrawalTransaction);
        accountRepository.save(accountMapper.AccountToAccountEntity(account));

        return TransactionStatus.createTransactionStatus(HttpStatus.OK, withdrawalTransaction.getApprovalCode());
    }

    @Transactional
    public TransactionStatus billPayment(String accountNumber, BillPaymentTransactionRequest billPaymentTransactionRequest) {
        this.validateTransactionRequest(billPaymentTransactionRequest);
        Account account = this.findAccount(accountNumber);
        BillPaymentTransaction billPaymentTransaction = this.transactionService.createBillPaymentTransaction(billPaymentTransactionRequest);
        account.post(billPaymentTransaction);
        accountRepository.save(accountMapper.AccountToAccountEntity(account));

        return TransactionStatus.createTransactionStatus(HttpStatus.OK, billPaymentTransaction.getApprovalCode());
    }


    private void fillTransaction(Transaction transaction, String transactionType){
        transaction.setType(transactionType);
        transaction.setApprovalCode(UUID.randomUUID().toString());
        transaction.setDate(new Date());
    }

    private void validateTransactionRequest(Transaction transaction){
        if (transaction.getAmount() < 0.1) {
            throw new InvalidParameterException("Invalid request parameter ammount can not lower than 0.1");
        }
    }

    private void validateTransactionRequest(BillPaymentTransactionRequest request){
        if (request.getAmount() < 0.1) {
            throw new InvalidParameterException("Invalid request parameter ammount can not lower than 0.1");
        }
    }


}
