package com.eteration.simplebanking.services;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;


@Service
public class UtilityService {

    private final Logger logger = LogManager.getLogger(this.getClass());

    public Class findClassByClassName(String className, String classPath) {
        try {
            String classPathWithName = new StringBuilder()
                    .append(classPath)
                    .append(".")
                    .append(className)
                    .toString();
            Class clazz = Class.forName(classPathWithName);
            return clazz;
        } catch (ClassNotFoundException e) {
            logger.error("ClassName or ClassPath NOT_FOUND!!", e);
        }
        return null;
    }

}
