package com.eteration.simplebanking.services;

import com.eteration.simplebanking.dto.BillPaymentTransactionRequest;
import com.eteration.simplebanking.model.BillPaymentTransaction;
import com.eteration.simplebanking.model.base.Constants;
import com.eteration.simplebanking.model.base.GeneralEnum;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Objects;
import java.util.UUID;

@Service
public class TransactionService {

    private final Logger logger = LogManager.getLogger(this.getClass());

    private final UtilityService utilityService;

    public TransactionService(UtilityService utilityService){
        this.utilityService = utilityService;
    }

    public BillPaymentTransaction createBillPaymentTransaction(BillPaymentTransactionRequest billPaymentTransactionRequest) {

        BillPaymentTransaction billPaymentTransaction =
                this.createBillPaymentTransactionInstanceByTransactionRelation
                        (billPaymentTransactionRequest.getBillType(), billPaymentTransactionRequest.getAmount());

        billPaymentTransaction.setType(GeneralEnum.TransactionTypes.BILLPAYMENT_TRANSACTION.getRelation());
        billPaymentTransaction.setBillTypePaymentParameter(billPaymentTransactionRequest.getBillTypePaymentParameter());
        billPaymentTransaction.setPayee(billPaymentTransactionRequest.getPayee());
        billPaymentTransaction.setApprovalCode(UUID.randomUUID().toString());
        billPaymentTransaction.setDate(new Date());
        return billPaymentTransaction;
    }

    public BillPaymentTransaction createBillPaymentTransactionInstanceByTransactionRelation(String className, double amount) {
        if (Objects.nonNull(className) && !className.isBlank()) {
            try {
                Class transactionClass = utilityService.findClassByClassName(className, Constants.TRANSACTION_PACKAGE_PATH);
                BillPaymentTransaction billPaymentTransaction = (BillPaymentTransaction) transactionClass.getConstructor(double.class).newInstance(amount);
                return billPaymentTransaction;
            } catch (Exception e) {
                logger.error("create BillingPaymentTransaction Instance Error!!", e);
            }
        }
        return null;
    }
}
